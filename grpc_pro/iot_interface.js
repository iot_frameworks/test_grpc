var PROTO_PATH = __dirname + '/../protos/iot.proto';
var grpc = require('grpc');
var path = require('path');
var messages = require('./iot_pb');
var services = require('./iot_grpc_pb');
var util = require('util');
var EventEmitter = require("events").EventEmitter;
var responseEvent = new EventEmitter();
var cl;


function nondis_ResponseObject()
{
	this.uuid;
	this.device;
}

function Property() 
{
	//var key = $(this).attr(name);
	//var value = $(this).attr(val);
	
	//this.property_name = name;
	//this.property_value = value;
	//this.map = map;
};

function Device(deviceid)
{
	this.Properties = [];
	this.dId = deviceid;
};

function Tthing()
{
	this.thingsName;
	this.osName;
	this.thingsType;
	this.hardwareVersion;
	this.manufacturerName;
	this.uuid;
	this.Devices = [];
};

function ResObject()
{
	this.Tthings = [];
};

function createlistenerport(add)
{
	var server = new grpc.Server();
	server.addService(services.SenderService,{discoveryResponse : discoveryResponse, nondiscoveryResponse:nondiscoveryResponse});
	server.bind(add,grpc.ServerCredentials.createInsecure());
	server.start();
}

function createsenderport(add)
{
	 cl = new services.SenderClient(add,grpc.credentials.createInsecure());
}

function request(json,callback)
{
	var s = new messages.RequestObject();
	var obj = JSON.parse(json);
	s.setCommand(obj.command);
	s.setUuid(obj.uuid);
	s.setDevid(obj.devid);
	s.setAttributeName(obj.attributename);
	s.setAttributeValue(obj.attributevalue);
	s.setMap(obj.map);
	cl.request(s,function(err,res) {
		var s = res.getMessage();
		callback(null,s);
	});
	
}

function discoveryResponse(call,callback)
{
	var res = new ResObject();
	var out = new messages.status();
	
	var tList = call.request.getTthingsList();
	for(var i=0 ; i<tList.length ; i++)
	{
		var t1 = new Tthing();
		t1.thingsName = tList[i].getThingsname();
		t1.osName = tList[i].getOsname();
		t1.thingsType = tList[i].getThingstype();
		t1.hardwareVersion = tList[i].getHardwareversion();
		t1.manufacturerName = tList[i].getManufacturername();
		t1.uuid = tList[i].getUuid();
		var dev_size = tList[i].getDevicesList().length;
		var devlist = tList[i].getDevicesList();
		for (var j=0;j<dev_size;j++)
		{
			var d = new Device(devlist[j].getDid());
			var pr_size = devlist[j].getPropertiesList().length;
			var prlist = devlist[j].getPropertiesList();
			for (var k=0; k< pr_size;k++)
			{
				var p = new Property();
				p[prlist[k].getPropertyName()] =prlist[k].getPropertyValue() 
				p['map'] = prlist[k].getPropertyMap();
				d.Properties.push(p);				
			}
			t1.Devices.push(d);
		}
		res.Tthings.push(t1);		
	}	
	out.setMessage(true);
	var json = JSON.stringify(res);
	responseEvent.emit('done_discovery_object', res);
	responseEvent.emit('done_discovery_json',json);
	callback(null,out);
}

function nondiscoveryResponse(call,callback)
{
	var go = new nondis_ResponseObject();
	var out = new messages.status();
	go.uuid = call.request.getUuid();
	var d = call.request.getDev();
	var pl = d.getPropertiesList();
	var de = new Device(d.getDid());
	for(var i=0;i<d.getPropertiesList().length;i++)
	{
		var p = new Property();
		
		var type = pl[i].getPropertyType();
		if(type === "int" || type === "boolean")
			p[pl[i].getPropertyName()] = parseInt(pl[i].getPropertyValue());
			else if (type === "double")
			p[pl[i].getPropertyName()] = parseFloat(pl[i].getPropertyValue());
		else 
			p[pl[i].getPropertyName()] = pl[i].getPropertyValue();
		
//		p[pl[i].getPropertyName()] = pl[i].getPropertyValue();
		p['map'] = pl[i].getPropertyMap();
		de.Properties.push(p);
	}
	go.device = de;
	out.setMessage(true);
	var json = JSON.stringify(go);
	responseEvent.emit('done_nondiscovery_object',go);
	responseEvent.emit('done_nondiscovery_json',json);
	callback(null,out);
}

exports.createlistenerport = createlistenerport;
exports.createsenderport = createsenderport;
exports.request = request;
exports.responseEvent = responseEvent;
