#include <iostream>
#include <memory>
#include <string>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <net/if.h>
#include<cstring>
#include <unistd.h>
#include<stdio.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <time.h>
#include <grpc++/grpc++.h>
#include <pthread.h>
#include <thread>
#include<future>
#include<vector>
#ifdef BAZEL_BUILD
#include "examples/protos/iot.grpc.pb.h"
#else
#include "iot.grpc.pb.h"
#endif

using grpc::Channel;
using grpc::ClientContext;
using grpc::ClientReader;
using grpc::ClientReaderWriter;
using grpc::ClientWriter;
using grpc::Status;
using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::ServerWriter;
using grpc::ServerReader;
using grpc::ServerReaderWriter;
using iot :: RequestObject;
using iot :: ResponseObject;
using iot :: NondiscoveryObject;
using iot :: status;
using iot::Sender;
using iot::tthing;
using iot::device;
using iot::tproperty;
using namespace std;

class client
{
	public:
	client(shared_ptr<Channel> channel):stub_(Sender::NewStub(channel)) {}
	bool DiscoveryResponse(const string& req)
	{
		
		ResponseObject res;
		tthing *t = res.add_tthings();

		device *d = t->add_devices();
		device *d1 = t->add_devices();
		tproperty *p11 = d->add_properties();
		tproperty *p12 = d ->add_properties();
		tproperty *p13 = d ->add_properties();
		tproperty *p21 = d1->add_properties();
		
		p11->set_property_name("value");
		p11->set_property_type("boolean");
		p11->set_property_value("0");
		p11->set_property_map("binaryswitch_2");
		
		p12->set_property_name("brightness");
		p12->set_property_type("int");
		p12->set_property_value("50");
		p12->set_property_map("brightness_2");
		d->set_did("SMLD1M_1");
		d1->set_did("SMFN1M_1");
		
		p13->set_property_name("dimmingSetting");
		p13->set_property_type("int");
		p13->set_property_value("50");
		p13->set_property_map("dimming_2");
		
		p21->set_property_name("value");
		p21->set_property_type("boolean");
		p21->set_property_value("0");
		p21->set_property_map("binaryswitch_1");
		
		
		t->set_thingsname("Modular_Fan_with_LED");
		t->set_osname("linux");
		t->set_thingstype("tronFAL01M");
		t->set_hardwareversion("1.1.1");
		t->set_manufacturername("Smartron");
		t->set_uuid("93000000-9d00-4000-6a00-000095000000");

		status reply; 
		ClientContext context;
		Status stat = stub_->DiscoveryResponse(&context,res,&reply);
		if(stat.ok())
		{
			return reply.message();
		}
		else
		{
			cout<<stat.error_code()<<":"<<stat.error_message()<<endl;
			return "RPC FAILED";
		}
	}
	bool NondiscoveryResponse(const RequestObject* r)
	{
		string command = r->command();
		string uuid = r->uuid();
		string devid = r->devid();
		NondiscoveryObject go;
		device *d = go.mutable_dev();
		tproperty *p11 = d->add_properties();
		d->set_did("SMLD1M_1");
		p11->set_property_name("value");
		p11->set_property_type("boolean");
		p11->set_property_value("0");
		p11->set_property_map("binaryswitch_2");
		go.set_uuid("93000000-9d00-4000-6a00-000095000000");
		
		status reply; 
		ClientContext context;
		Status stat = stub_->NondiscoveryResponse(&context,go,&reply);
		if(stat.ok())
		{
			return reply.message();
		}
		else
		{
			cout<<stat.error_code()<<":"<<stat.error_message()<<endl;
			return "RPC FAILED";
		}
		
	}
	
	private:
	unique_ptr<Sender::Stub> stub_;
};

client c(grpc::CreateChannel("192.168.0.119:1111",grpc::InsecureChannelCredentials()));    	

void response_method(const RequestObject* ro)
{
	bool stats;
	string st = ro->command();
	if(st == "discovery")
		stats = c.DiscoveryResponse(st);
	else
		stats = c.NondiscoveryResponse(ro);
	cout << stats <<endl;
}

class serviceImpl final : public Sender::Service 
{
	Status Request(ServerContext* context, const RequestObject* ro,
                    status* stats) override 
    {
    	if(ro->command() == "get"|| ro->command() == "post" || ro->command() == "discovery")
    	{   	
    		stats->set_message(true);
    		thread([ro](){response_method(ro);}).detach();
    	}
    	else
    		stats->set_message(false);
    	return Status::OK;		   	
    }
};

void runserver()
{
	string server_add("192.168.0.167:2222");
	serviceImpl service;
	ServerBuilder builder;
  builder.AddListeningPort(server_add, grpc::InsecureServerCredentials());
  builder.RegisterService(&service);
  unique_ptr<Server> server(builder.BuildAndStart());
  cout << "Server listening on " << server_add <<endl;
  server->Wait();
}

int main(int argc, char** argv) {
  
  runserver();
  return 0;
}
