// GENERATED CODE -- DO NOT EDIT!

'use strict';
var grpc = require('grpc');
var iot_pb = require('./iot_pb.js');

function serialize_iot_NondiscoveryObject(arg) {
  if (!(arg instanceof iot_pb.NondiscoveryObject)) {
    throw new Error('Expected argument of type iot.NondiscoveryObject');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_iot_NondiscoveryObject(buffer_arg) {
  return iot_pb.NondiscoveryObject.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_iot_RequestObject(arg) {
  if (!(arg instanceof iot_pb.RequestObject)) {
    throw new Error('Expected argument of type iot.RequestObject');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_iot_RequestObject(buffer_arg) {
  return iot_pb.RequestObject.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_iot_ResponseObject(arg) {
  if (!(arg instanceof iot_pb.ResponseObject)) {
    throw new Error('Expected argument of type iot.ResponseObject');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_iot_ResponseObject(buffer_arg) {
  return iot_pb.ResponseObject.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_iot_status(arg) {
  if (!(arg instanceof iot_pb.status)) {
    throw new Error('Expected argument of type iot.status');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_iot_status(buffer_arg) {
  return iot_pb.status.deserializeBinary(new Uint8Array(buffer_arg));
}


var SenderService = exports.SenderService = {
  request: {
    path: '/iot.Sender/Request',
    requestStream: false,
    responseStream: false,
    requestType: iot_pb.RequestObject,
    responseType: iot_pb.status,
    requestSerialize: serialize_iot_RequestObject,
    requestDeserialize: deserialize_iot_RequestObject,
    responseSerialize: serialize_iot_status,
    responseDeserialize: deserialize_iot_status,
  },
  discoveryResponse: {
    path: '/iot.Sender/DiscoveryResponse',
    requestStream: false,
    responseStream: false,
    requestType: iot_pb.ResponseObject,
    responseType: iot_pb.status,
    requestSerialize: serialize_iot_ResponseObject,
    requestDeserialize: deserialize_iot_ResponseObject,
    responseSerialize: serialize_iot_status,
    responseDeserialize: deserialize_iot_status,
  },
  nondiscoveryResponse: {
    path: '/iot.Sender/NondiscoveryResponse',
    requestStream: false,
    responseStream: false,
    requestType: iot_pb.NondiscoveryObject,
    responseType: iot_pb.status,
    requestSerialize: serialize_iot_NondiscoveryObject,
    requestDeserialize: deserialize_iot_NondiscoveryObject,
    responseSerialize: serialize_iot_status,
    responseDeserialize: deserialize_iot_status,
  },
};

exports.SenderClient = grpc.makeGenericClientConstructor(SenderService);
