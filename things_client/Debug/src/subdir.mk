################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CC_SRCS += \
../src/iot.grpc.pb.cc \
../src/iot.pb.cc 

CPP_SRCS += \
../src/hirediswrite.cpp \
../src/iot_server.cpp \
../src/iotivity_property.cpp \
../src/t_device.cpp \
../src/t_discovery.cpp \
../src/t_property.cpp \
../src/t_resource.cpp \
../src/t_things.cpp 

CC_DEPS += \
./src/iot.grpc.pb.d \
./src/iot.pb.d 

OBJS += \
./src/hirediswrite.o \
./src/iot.grpc.pb.o \
./src/iot.pb.o \
./src/iot_server.o \
./src/iotivity_property.o \
./src/t_device.o \
./src/t_discovery.o \
./src/t_property.o \
./src/t_resource.o \
./src/t_things.o 

CPP_DEPS += \
./src/hirediswrite.d \
./src/iot_server.d \
./src/iotivity_property.d \
./src/t_device.d \
./src/t_discovery.d \
./src/t_property.d \
./src/t_resource.d \
./src/t_things.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -std=c++0x -I../../iotivity-1.2.1/resource/csdk/stack/include -I../../grpc/examples/cpp/grpc_pro/include -I../../iotivity-1.2.1/resource/oc_logger/include -I../../iotivity-1.2.1/resource/include -I../../iotivity-1.2.1/resource/c_common -I../../iotivity-1.2.1/resource/c_common/ocrandom/include -I../../iotivity-1.2.1/resource/csdk/logger/include -I../../hiredis -I../../grpc/include/grpc -I../../grpc/include/grpc++ -I../../things_client/include -Os -g3 -Wall -Wextra -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/%.o: ../src/%.cc
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -std=c++0x -I../../iotivity-1.2.1/resource/csdk/stack/include -I../../grpc/examples/cpp/grpc_pro/include -I../../iotivity-1.2.1/resource/oc_logger/include -I../../iotivity-1.2.1/resource/include -I../../iotivity-1.2.1/resource/c_common -I../../iotivity-1.2.1/resource/c_common/ocrandom/include -I../../iotivity-1.2.1/resource/csdk/logger/include -I../../hiredis -I../../grpc/include/grpc -I../../grpc/include/grpc++ -I../../things_client/include -Os -g3 -Wall -Wextra -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


