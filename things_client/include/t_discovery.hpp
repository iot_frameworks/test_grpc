
#ifndef DISCOVERY_H_
#define DISCOVERY_H_

/*    FoundResource :
    FoundResource method creates the resource instances.
    Depending on the resources found, it ignores some of the URI's like "/oic/sec/doxm", "/oic/sec/pstat", "/oic/d", "/oid/p", "oic/d", "oic/p"
    Differentiates the collection uri's and resources uri's depending on the interface and store them in saperate maps.
    t_resourceColmap : holds the collection resource against its key.
    t_resourcemap    : holds the non-collection resource against its key.
    where key is (uuid+uri)
*/
  void foundResource (std::shared_ptr<OCResource> resource);

/*    DiscoveryFun :
    discoveryFun finds the resources in the OC_RSRVD_WELL_KNOWN_URI by invoking
    findResource() function with a callback function foundResource().
*/
  void resource_discovery ();

/*  ReceivedDeviceInfo :
    It creates a new thing class instance.
    -- Gets the info such as "di"(uuid), "n"(thingsName), "dt"(thingsType/deviceType)
    -- Stores the instance in t_thingsmap.
    t_thingsmap : holds the tthings class instance against its uuid.
*/
  void receivedDeviceInfo (const OCRepresentation& rep);

/*  DeviceInfo :
    deviceInfo multicasts and gets the DeviceInfo of tthings available on
    /oic/d, by invoking getDeviceInfo() function with a callback function receivedDeviceInfo().
*/
  void deviceInfo ();

/*  ReceivedPlatformInfo :
    - Using the uuid(pi) available in the representation received, finds the tthing by mapping uuid in the t_thingsmap.
    - If the uuid available in the t_thingsmap, then gets the platformInfo for that tthings instance.
    - Gets the info such as "mnmn"(manufacturerName), "mnos"(osName), "mnhw"(hardwareVersion), "mnfv"(firmwareVersion).
*/
  void receivedPlatformInfo (const OCRepresentation& rep);

/*  PlatformInfo :
  platformInfo multicasts and gets the platform information of tthings available
  on /oic/p, by invoking getPlatformInfo() function with a callback function receivedPlatformInfo().
*/
  void platformInfo ();

/*  DiscoveryAll :
  	Responsible for discovering deviceInfo, platfromInfo and resources.
*/
  void Discovery_all ();

/*  TthingsGarbage :
    -- Erases the unnecessary TThing instances.
*/
  void t_things_garbage ();

/*  TThingsDevice :
    -- This method creates TDevice instances and inserts the resources in the resMap (map).
    -- Sets the callback "SetResourceCallback"
    -- Push back the device instance to a DeviceVec vector.
*/
  void t_things_device ();

/*  TResourceAttDetails
    -- This method makes "get" on all the resource instances in ResMap.
*/
  void t_resource_att_details ();

/*  EndDiscovery
    -- This method internally invokes t_things_garbage, t_things_device();
        t_resource_att_details() methods.
    -- And sets the alarm signal depending on the auto_discovery flag.

*/
  void end_Discovery (int sig);


#endif /* DISCOVERY_H_ */
