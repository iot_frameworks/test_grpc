/*
 * iot_server.h
 *
 *  Created on: 18-Jun-2017
 *      Author: jotirling
 */

#ifndef INCLUDE_IOT_SERVER_H_
#define INCLUDE_IOT_SERVER_H_

#include <iostream>
#include <memory>
#include <string>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <net/if.h>
#include<cstring>
#include <unistd.h>
#include<stdio.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <time.h>
#include <grpc++/grpc++.h>
#include <pthread.h>
#include <thread>
#include<future>
#include<vector>
#ifdef BAZEL_BUILD
#include "examples/protos/iot.grpc.pb.h"
#else
#include "iot.grpc.pb.h"
#endif

using grpc::Channel;
using grpc::ClientContext;
using grpc::ClientReader;
using grpc::ClientReaderWriter;
using grpc::ClientWriter;
using grpc::Status;
using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::ServerWriter;
using grpc::ServerReader;
using grpc::ServerReaderWriter;
using iot::RequestObject;
using iot::ResponseObject;
using iot::NondiscoveryObject;
using iot::status;
using iot::Sender;
using iot::tthing;
using iot::device;
using iot::tproperty;
using namespace std;

void request_method(const RequestObject* r);
void runserver(string server_add);

void sendrequest(struct GrpcRequest reqStruct);

class client {

public:

  // Client constructor it requires a channel, out of which the actual RPCs
  // are created. This channel models a connection to an endpoint (in this case,
  // localhost at port 50051). We indicate that the channel isn't authenticated
  // (use of InsecureChannelCredentials()).
	client(shared_ptr<Channel> channel) :
			stub_(Sender::NewStub(channel)) {
	}
   //this function receive the tthing object from iotivity client , 
   // It assembles the iotivity-client's payload, sends it node js client.
  
	bool DiscoveryResponse(std::map<string, t_things*> t_thingsmapobj); 
	
   //this function receive the tdevice object from iotivity client , 
   // It assembles the iotivity-client's payload, sends it node js client.
	bool NondiscoveryResponse(t_device* t_deviceObjRes);

private:
	unique_ptr<Sender::Stub> stub_;
};

#endif /* INCLUDE_IOT_SERVER_H_ */
