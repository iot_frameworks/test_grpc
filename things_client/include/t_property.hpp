
#ifndef PROPERTY_H_
#define PROPERTY_H_

using namespace OC;
using namespace std;


/* property structure : structure which is used to hold the details of properties(attributes) of a resource.
	-- It holds the name and type of property.
	-- "map" member is used to hold the name of the resource.
	-- Union to store the value of the property depending on the type.
*/
struct property
{
	char * name;
	int type;
	string map;

	union
	{
		int64_t i;
		double d;
		bool b;
		char *   str;
	};
	struct property *next;
};


/* TProperty class : Used to create a property node and show it.
	-- createnode() : This member function is used to create a new node for each and every property of a resource.
	-- show() : This member function is used to show the name, type, value of a property.
*/
class t_property
{
	public:
		struct property *head;

		void createnode( );
		void Display( );
};



#endif /* #define PROPERTY_H_ */
