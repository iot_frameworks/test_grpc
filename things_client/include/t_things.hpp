#ifndef THINGS_H_
#define THINGS_H_

using namespace OC;
using namespace std;




/* TThings class: Holds Tthing details.
    -- aThingsName       - holds thing name.
    -- aThingsType       - holds thing type.
    -- aUuid             - holds uuid.
    -- aManufacturerName - holds manufacturerName.
    -- aOsName           - holds OsName.
    -- aFirmwareVersion  - holds FirmwareVersion.
    -- aHardwareVersion  - holds HardwareVersion.
    -- DeviceVec is a vector that holds TDevice instances.
    -- setTdeviceCall()  - member function used to set the callback to update Tthings. (any change in Tdevice will update Tthings)
    -- notifyDevice()	 - This member function gets the update from Tdevice regarding changes in properties. And then it sends Tthings object through "grpc" to PDA.
*/
class t_things
{
public:
string thingsName;
string thingsType;

string uuid;
string manufacturerName;
string osName;
string firmwareVersion;
string hardwareVersion;

std::vector<t_device*> deviceVec;

void setTdeviceCall();
void notifyDevice();

void Display( );

};




#endif /* #define THINGS_H_ */
