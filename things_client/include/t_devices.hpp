
#ifndef DEVICES_H_
#define DEVICES_H_

/* Device class: Holds device details.
	-- ColUri holds the URI of the collection resource.
	-- ResMap holds a map(uuid+uri) of key and resource class instance to store the non-collection resource instances.
  -- ProData gives the property details under each device.
  -- TDevice() is a constructor
  -- SetResourceCallback() - member function used to set the callback to update the properties recieved in get.
  -- Notify() - Depending on the representation received and depending on the request the respective action is performed.
  -- Display() - displays the details of properties.
  -- devUpdateCall is a typedef callback function.
  -- devCallback is devUpdateCall type variable used to SetDeviceCallback.
*/

using namespace OC;
using namespace std;

class t_device
{
public:
string Coluri;
std::map<string, t_resource*> resMap;
t_property pro_data;

typedef std::function<void( )>devUpdateCall;
devUpdateCall devCallback;

t_device();
void setresourcecallback();
void notify(OCRepPayloadValue* repp,string request,string uri,int flag);
void setDeviceCallback(devUpdateCall devCallback);
void t_device_display();
};



#endif /* #define DEVICES_H_ */
