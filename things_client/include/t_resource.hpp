
// #include "devices.h"

#ifndef RESOURCE_H_
#define RESOURCE_H_

using namespace OC;
using namespace std;

/* AttributeType structure: Holds value of the property depending on its type */
struct attributetype
{
        int i;
        double d;
        bool b;
        string str;
};



/* TResouce class: Holds TResouce details.
    -- aResourceUri       - holds ResourceUri.
    -- aServerId       - holds Uuid.
    -- akey             - holds Key (serverID + ResourceUri).
    -- notifycallback is a typedef function.
    -- callback is a notifycallback variable bind with Notify() function
    of TDevice class to update the representation got in OnGet for a resource.
    -- resource is a shared pointer of OCResource class.
    -- Attdetals is OCRepPayloadValue type variable which is used to provide attribute details (type, name, value)
    -- OnGet is callback defined for GET functionality.
    -- getRepresentation() method is used to invoke "get" for a particular resource.
    -- OnPost is callback defined for POST functionality.
    -- postRepresentation() method is used to invoke "post" for a particular resource.
    -- onObserve is callback defined for OBSERVE functionality.
    -- observeRepresentation() method is used to request "observe" functionality for a particular resource.
    -- setnotifycallback() is used to bind with Notify() function.
    -- Display() to displays the details of TResource.
*/
class t_resource
{
	public:
		std::string resourceURI;
		std::string serverID;
		std::string key;
		typedef std::function<void(OCRepPayloadValue *,string,string,int)> notifycallback;
		notifycallback callback;

		std::shared_ptr<OCResource> resource;
		OCRepPayloadValue* Attdetals;

		void onGet(const HeaderOptions& /*headerOptions*/, const OCRepresentation& rep, const int eCode);
		void getRepresentation( );
		void onPost(const HeaderOptions& /*headerOptions*/, const OCRepresentation& rep, const int eCode);
		void postRepresentation( OCRepresentation rep);
		void onObserve(const HeaderOptions /*headerOptions*/, const OCRepresentation& rep,
		               const int& eCode, const int& sequenceNumber);
		void observeRepresentation( );


		void setnotifycallback(notifycallback callback);

		void t_resource_diplay();

};

#endif /* #define RESOURCE_H_ */
