/*
 * SAMPLEGRPC.h
 *
 *  Created on: 09-Jun-2017
 *      Author: smartron
 */

#ifndef SAMPLEGRPC_H_
#define SAMPLEGRPC_H_

/* GRPC structure: Holds GRPC details.
    -- command       	- command (discovery/control/get/post/observe).
    -- UUID     		- Uuid of TThing
    -- DevID	        - Device ID.
    -- map 			 	- resource name(resource uri).
    -- attributeName    - attribute name.
    -- attributeValue   - attribute value.
*/
struct GrpcRequest
{
	std::string command;
	std::string UUID;
	std::string DevID;
	std::string map;
	std::string attributeName1;
	std::string attributeValue1;

};


#endif /* SAMPLEGRPC_H_ */
