/*
 * hirediswrite.cpp
 *
 *  Created on: 13-Jun-2017
 *      Author: jotirling
 */
#include "headers.hpp"

redisContext *c2;
redisReply *reply;
const char *hostname = "0.0.0.0";
int port = 6379;
std::string space = " ";
extern std::map<std::string, int> RepPayloadMap;

void REDIS::Context(void) {
	c2 = redisConnect(hostname, port);
	if (c2 == NULL || c2->err) {
		if (c2) {
			printf("Connection error: %s\n", c2->errstr);
			redisFree(c2);
		} else {
			printf("Connection error: can't allocate redis context\n");
		}
		exit(1);
	}
}

void REDIS::WriteRedisThings(std::string hash_key, std::string discinfo) {
	REDIS hash;
	std::string key = "uuid_" + hash_key;
	std::string space = " ";

	stringstream stringAtrVal;
	std::string buffer = "";
	redisReply *reply1;   //redis Message
	hash.Context();   //redis connectio to write in to db

	string res1 = "HMSET " + key + space + discinfo;
	std::cout << res1 << endl;
	int TempNumOne = res1.size();
	char res[1000];
	for (int a = 0; a <= TempNumOne; a++) {
		res[a] = res1[a];
	}
	reply1 = (redisReply *) redisCommand(c2, res);
	printf("HMSET :: %s\n", reply1->str);
	printf("IPADDRESS of DB :: %s\n", hostname);

	freeReplyObject(reply1);
	redisFree(c2);

}

void REDIS::WriteRedisDevice(string hash_key, OCRepPayloadValue* repp) {

	REDIS hash;
	std::string key = "status_" + hash_key;

	if (repp == NULL) {
		std::cout << "/* message */" << '\n';
	}
	while (repp) {

		stringstream stringAtrVal;
		std::string buffer = "";
		redisReply *reply1; //redis Message
		hash.Context(); //redis connectio to write in to db

		int datatype = repp->type;
		char* names = repp->name;
		std::string name(names);

		RepPayloadMap.insert(std::pair<std::string, int>(name, datatype));

		cout << "DataType :" << datatype << endl;
		switch (datatype) {
		case 0:
			cout << "OCREP_PROP_NULL " << endl;
			break;
		case 1: {
//                        cout << "OCREP_PROP_INT " <<endl;
//                        cout << "AttrName :" << name <<endl;
//                        cout << "AttrValue:" << repp->i <<endl;

			stringAtrVal << repp->i;
			string str = stringAtrVal.str();
			buffer = name + space + str;
		}
			break;
		case 2: {
//                        cout << "OCREP_PROP_DOUBLE  " <<endl;
//                        cout << "AttrName :" << name <<endl;
//                        cout << "AttrValue:" << repp->d <<endl;

			stringAtrVal << repp->d;
			string str = stringAtrVal.str();
			buffer = name + space + str;
		}
			break;

		case 3: {
//                        cout << "OCREP_PROP_BOOL  " <<endl;
//                        cout << "AttrName :" << name <<endl;
//                        cout << "AttrValue:" << boolalpha << repp->b <<endl;

			stringAtrVal << repp->b;
			string str = stringAtrVal.str();
			buffer = name + space + str;
		}
			break;

		case 4: {
//                        cout << "OCREP_PROP_STRING  " <<endl;
//                        cout << "AttrName :" << name <<endl;
//                        cout << "AttrValue:" << repp->str <<endl;

			stringAtrVal << repp->str;
			string str = stringAtrVal.str();
			buffer = name + space + str;
		}
			break;

		case 5:
			cout << "OCREP_PROP_BYTE_STRING" << endl;
			break;
		case 6:
			cout << "OCREP_PROP_OBJECT" << endl;
			break;
		case 7:
			cout << "OCREP_PROP_ARRAY" << endl;
			break;

		}

		string res1 = "HMSET " + key + space + buffer;
		int TempNumOne = res1.size();
		char res[1000];
		for (int a = 0; a <= TempNumOne; a++) {
			res[a] = res1[a];
		}

		reply1 = (redisReply *) redisCommand(c2, res);
		std::cout << res << endl;
		printf("HMSET :: %s", reply1->str);
		printf("\nIPADDRESS of DB :: %s\n", hostname);

		freeReplyObject(reply1);
		redisFree(c2);			//free the created connection of redis-client......

		repp = repp->next;

	}
	std::cout << '\n';
}
