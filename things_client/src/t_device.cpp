/*
 * t_device.cpp
 *
 *  Created on: 19-Jun-2017
 *      Author: jotirling
 */
#include "headers.hpp"

using namespace std;

int DisFlag=0;

//REDIS hiredisdev;
t_device::t_device() {
	Coluri = "";
	pro_data.head = NULL;
}

void t_device::t_device_display()
{
	cout << "Coluri :" <<this->Coluri  <<endl;

      for (auto it =this->resMap.begin();it!=this->resMap.end();it++)
  		std::cout << it->first 	<< " => " << it->second 	<< '\n';

	cout << "map uri:" <<this->pro_data.head->map<<endl;
	cout << "Property Data: "<<endl;

	this->pro_data.Display();

}

void t_device::setDeviceCallback(devUpdateCall devCallback)
{
  this->devCallback=devCallback;
}


void t_device::setresourcecallback()
{

    for (auto it =this->resMap.begin();it!=this->resMap.end();it++)
    {
	it->second->setnotifycallback(std::bind( &t_device::notify, this, _1,_2,_3,_4));
    }
}


void t_device::notify(OCRepPayloadValue* repp,string request,string uri,int flag)
{

    OCRepPayloadValue* rep=repp;
    char * name;
    struct property *temp ;


	while (rep)
	  {
	name=rep->name;

	if(request == "discovery")
	  {

	    this->pro_data.createnode( );
	    this->pro_data.head->type = rep->type;
	    this->pro_data.head->name = rep->name;
	    this->pro_data.head->map =uri;
	    this->pro_data.head->map.erase(0,47);

	  }

	temp=this->pro_data.head;

	while (temp)
	  {
	    if(strcmp(temp->name,name)==0)
	      {
 		switch ( temp->type )
		{

		  case 0:
			  cout << "OCREP_PROP_NULL " << endl;
			  break;

		  case 1:
			  temp->i =rep->i;
			  cout<<"Value Posted: "<<temp->i<<endl;
			  break;

		  case 2:
			  temp->d =rep->d;
			  break;

		  case 3:
			  temp->b =rep->b;
	                  break;

	          case 4:

			  temp->str =rep->str;
		          break;

	          default:
			  cout<<"in default"<<endl;
		 }
	      }

	    temp=temp->next;
	  }
        rep = rep->next;

      }

      if( (request == "get") && (flag == this->resMap.size()) )
	{
    	  cout << "sending the get response to GRPC Server ................."
    	  				<< endl;
    	  		//To call service methods, we first need to create a stub.
    	  		//First we need to create a gRPC channel for our stub, specifying the server address and port we want to connect to and any special channel arguments - in our case we’ll use the default ChannelArguments and no SSL:
    	  		//we can use the channel to create our stub using the NewStub method
    	  		client grpcClient(
    	  				grpc::CreateChannel("localhost:1111",
    	  						grpc::InsecureChannelCredentials()));
    	  		grpcClient.NondiscoveryResponse(this);
	}
      else if(request == "post")
	{
	cout << "sending the post response to GRPC Server ................."
				<< endl;
		//To call service methods, we first need to create a stub.
		//First we need to create a gRPC channel for our stub, specifying the server address and port we want to connect to and any special channel arguments - in our case we’ll use the default ChannelArguments and no SSL:
		//we can use the channel to create our stub using the NewStub method
		client grpcClient(
				grpc::CreateChannel("localhost:1111",
						grpc::InsecureChannelCredentials()));
		grpcClient.NondiscoveryResponse(this);

	}
      else if( (request == "discovery") )
	{
	  DisFlag++;
	  this->devCallback();
	}

}

