/*
 * t_things.cpp
 *
 *  Created on: 20-Jun-2017
 *      Author: jotirling
 */
#include "headers.hpp"

using namespace OC;
using namespace std;
REDIS hiredisget;
extern std::map<string, t_resource*> t_resourcemap;
extern std::map<string, t_things*> t_thingsmap;                  // t_things map

extern int DisFlag;

void t_things::Display() {

	std::string values[] = { " thingsName ", this->thingsName, " thingsType ",
			this->thingsType, " manufacturerName ", this->manufacturerName,
			" osName ", this->osName, " hardwareVersion ",
			this->hardwareVersion, " firmwareVersion ", this->firmwareVersion, };

	for (unsigned int i = 0; i < sizeof(values) / sizeof(values[0]); i += 2) {
		std::cout << values[i] << " : " << values[i + 1] << endl;
		std::string info = values[i] + values[i + 1];
		hiredisget.WriteRedisThings(this->uuid, info);
	}

	for (int i = 0; i < this->deviceVec.size(); i++) {
		this->deviceVec[i]->t_device_display();
		cout << endl;
	}

}

void t_things::setTdeviceCall() {
	for (int i = 0; i < this->deviceVec.size(); i++) {
		deviceVec[i]->setDeviceCallback(
				std::bind(&t_things::notifyDevice, this));
	}
}

void t_things::notifyDevice() {
	cout << DisFlag << " /t " << t_resourcemap.size() << endl;
	if (DisFlag == t_resourcemap.size()) {
		cout << "disc done \n\n";

		std::cout << " \n t_things Map: " << '\n';
		for (auto it = t_thingsmap.cbegin(); it != t_thingsmap.cend(); ++it) {
			it->second->Display();
		}

		//To call service methods, we first need to create a stub.
		//First we need to create a gRPC channel for our stub, specifying the server address and port we want to connect to and any special channel arguments - in our case we’ll use the default ChannelArguments and no SSL:
		//we can use the channel to create our stub using the NewStub method

		client grpcClient(
				grpc::CreateChannel("localhost:1111",
						grpc::InsecureChannelCredentials()));
		grpcClient.DiscoveryResponse(t_thingsmap);

		DisFlag = 0;
	}
}

