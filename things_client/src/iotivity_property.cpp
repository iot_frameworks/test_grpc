#include "headers.hpp"

using namespace OC;
using namespace std;

#define  discovering   1181
#define  getting        754
#define  posting        772
#define  observeing     1076
#define  print          557

void Auto_Discovery(int);
int getdone = 0;

extern std::map<string, t_things*> t_thingsmap;                  // t_things map
extern bool auto_Discovery;
string reqType;
t_device *t_deviceobj1;

void getResource(string uuid, string devid)
{
	QueryParamsMap test;

	cout << "In function getResource" << endl;
	getdone = 0;

	auto it_uid = t_thingsmap.find(uuid);
	if (it_uid != t_thingsmap.end())
	{
		for (int i = 0; i < it_uid->second->deviceVec.size(); i++)
		{
			if (devid.compare(it_uid->second->deviceVec[i]->Coluri) == 0)
			{
				for (auto it_key =it_uid->second->deviceVec[i]->resMap.cbegin();
						it_key != it_uid->second->deviceVec[i]->resMap.cend();++it_key)
				{
					if (it_key != it_uid->second->deviceVec[i]->resMap.end())
					{
						std::cout << "get req sent :" << endl;
						it_key->second->getRepresentation();
					}
				}
			}
		}
	}
}

void postResource(string uuid, string devid, string key, string attributeName,
		string inputValue) {
	attributetype attributeValue;
	OCRepresentation rep;
	struct property *temp;

	std::map<string, t_resource*>::iterator resItr;

	auto it_uid = t_thingsmap.find(uuid);
	if (it_uid != t_thingsmap.end()) {
		for (int i = 0; i < it_uid->second->deviceVec.size(); i++) {
			if (devid.compare(it_uid->second->deviceVec[i]->Coluri) == 0) {

				auto it = it_uid->second->deviceVec[i]->resMap.find(key);
				if (it != it_uid->second->deviceVec[i]->resMap.end()) {
					cout << "In resMap with uri: " << it->first << endl;
					temp = it_uid->second->deviceVec[i]->pro_data.head;

					while (temp) {
						string name = string(temp->name);
						if (name.compare(attributeName) == 0) {

							switch (temp->type) {

							case 0:
								cout << "OCREP_PROP_NULL" << endl;
								break;
							case 1:
								cout << "OCREP_PROP_INT" << endl;
								attributeValue.i = stoi(inputValue);
								rep.setValue(attributeName, attributeValue.i);
								break;
							case 2:
								cout << "OCREP_PROP_DOUBLE" << endl;
								attributeValue.d = stod(inputValue);
								rep.setValue(attributeName, attributeValue.d);
								break;
							case 3:
								cout << "OCREP_PROP_BOOL" << endl;
								if (inputValue == "true")
									attributeValue.b = 1;
								else if (inputValue == "false")
									attributeValue.b = 0;
								rep.setValue(attributeName, attributeValue.b);
								break;
							case 4:
								cout << "OCREP_PROP_STRING" << endl;
								attributeValue.str = inputValue;
								rep.setValue(attributeName, attributeValue.str);
								break;
							}

							it->second->postRepresentation(rep);
						}
						temp = temp->next;
					}
				}
			}
		}
	}
}

void observeResource(string uuid, string key) {
	cout << "In function observeResource" << endl;
	QueryParamsMap test;
	auto it_uid = t_thingsmap.find(uuid);
	if (it_uid != t_thingsmap.end()) {
		for (int i = 0; i < it_uid->second->deviceVec.size(); i++) {
			auto it = it_uid->second->deviceVec[i]->resMap.find(key);
			if (it != it_uid->second->deviceVec[i]->resMap.end()) {
				it->second->observeRepresentation();
			}
		}
	}
}

int convertToASCII(string letter) {

	int m = 0;
	for (int i = 0; i < letter.length(); i++) {
		char x = letter.at(i);
		m += int(x);
	}
	return m;
}

void one_time_Discovery() {
	cout << "In one_time_Discovery" << endl;
	signal(SIGALRM, end_Discovery);
	Discovery_all();
}

void Auto_Discovery(int sig) {
	printf("IN Auto_Discovery\n");
	one_time_Discovery();
//      alarm(3600);
}

void DisRequest(int type) {

	printf("IN DisRequest\n");

	switch (type)
	{
	case 1:
		std::cout << "in one_time_Discovery" << '\n';
		one_time_Discovery();
		break;

	case 2:
		signal(SIGALRM, Auto_Discovery);
		auto_Discovery = true;
		std::cout << "in Auto_Discovery" << '\n';
		alarm(1);
		break;

	case 3:
		signal(SIGALRM, SIG_IGN);
		auto_Discovery = false;
		std::cout << "in Disable_Auto_Discovery" << '\n';
		break;

	default:
		std::cout << "in one_time_Discovery" << '\n';
		one_time_Discovery();
		break;
	}

	printf("OUT myThread_Dis\n");

}

void ResControl(struct GrpcRequest control)
{

	int option = convertToASCII(control.command);

		string attributeName,attributeValue;
		string key,uuid,devid;
		uuid=control.UUID;
		devid=control.UUID+control.DevID;
		key=devid+"/"+control.map;
		attributeName=control.attributeName1;
		attributeValue=control.attributeValue1;

	switch (option)
	{
	case getting:
		{
		reqType = "get";
		getResource(uuid, devid);
		}
		break;

	case posting:
		{
		reqType = "post";
		postResource(uuid, devid, key, attributeName,attributeValue);
		}
		break;

	case observeing:
		{
		observeResource(uuid, devid);
		}
		break;
	}
}

void sendrequest(struct GrpcRequest reqStruct)
{
	cout << "In send request IotivityClient" << endl;
	int init = 1;
	int request = convertToASCII(reqStruct.command);

	switch (request)
	{
	case discovering:
		thread([init]() {DisRequest(init);}).detach();
		break;

	default:
		thread([reqStruct]() {ResControl(reqStruct);}).detach();
		break;
	}

}

int main()
	{
	std::cout.setf(std::ios::boolalpha);
	int init = 1;

	thread([init]() {DisRequest(init);}).detach();

	std::cout << "\nStarting the GRPC Server " << endl;

	runserver("0.0.0.0:2222");//sddress of GRPC server

	return 0;
}
