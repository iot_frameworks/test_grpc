#include "headers.hpp"

void Auto_Discovery(int);

using namespace OC;
using namespace std;

std::map<string, t_resource*> t_resourcemap;
std::map<string, t_resource*>::iterator t_resourcemapItr;      // t_resource map

std::map<string, t_resource*> t_resourceColmap;
std::map<string, t_resource*>::iterator t_resourceColmapItr; // t_resourceColmap map

std::map<string, t_things*> t_thingsmap;                         // t_things map

static OCConnectivityType connectivityType = CT_ADAPTER_IP;

std::map<std::string, int> RepPayloadMap;

bool auto_Discovery = false;
REDIS hiredis;
extern string reqType ;


void foundResource(std::shared_ptr<OCResource> resource) {
	t_resource *temp = new t_resource;

	temp->resource = resource;
	string hostAddress = resource->host();
	temp->resourceURI = resource->uri();
	temp->serverID = resource->sid();
	temp->key = temp->serverID + temp->resourceURI;

	if (!temp->resourceURI.compare("/oic/sec/doxm") == 0
			&& !temp->resourceURI.compare("/oic/sec/pstat") == 0
			&& !temp->resourceURI.compare("/oic/d") == 0
			&& !temp->resourceURI.compare("/oic/p") == 0
			&& !temp->resourceURI.compare("oic/d") == 0
			&& !temp->resourceURI.compare("oic/p") == 0) {
		std::vector<std::string> interfaceVec;
		for (auto &resourceInterfaces : temp->resource->getResourceInterfaces()) {
			interfaceVec.push_back(resourceInterfaces);
		}

		if (std::find(interfaceVec.begin(), interfaceVec.end(), "oic.if.ll")
				!= interfaceVec.end()) {

			t_resourceColmap[temp->key] = temp;

		} else {
			t_resourcemap[temp->key] = temp;
		}

	}
	alarm(1);
}

void resource_discovery()
{
	std::ostringstream requestURI;

	cout << "In function Discovery" << endl;

	requestURI << OC_RSRVD_WELL_KNOWN_URI; // << "?rt=oic/res";

	OCPlatform::findResource("", requestURI.str(), CT_DEFAULT, &foundResource);

}

void receivedDeviceInfo(const OCRepresentation& rep)
{
	t_things *temp = new t_things;
	rep.getValue("di", temp->uuid);
	rep.getValue("n", temp->thingsName);
	rep.getValue("dt", temp->thingsType);

	std::string thingsName = " thingsName " + temp->thingsName;
	std::string thingsType = " thingsType " + temp->thingsType;

	t_thingsmap[temp->uuid] = temp;
}

void deviceInfo()
{
	cout << "In function deviceInfo " << endl;
	std::string deviceDiscoveryURI = "/oic/d";
	OCPlatform::getDeviceInfo("", deviceDiscoveryURI, connectivityType,
			&receivedDeviceInfo);
}

void receivedPlatformInfo(const OCRepresentation& rep)
{
	std::string PlatId;
	rep.getValue("pi", PlatId);

	auto it = t_thingsmap.find(PlatId);
	if (it != t_thingsmap.end()) {
		rep.getValue("mnmn", it->second->manufacturerName);
		rep.getValue("mnos", it->second->osName);
		rep.getValue("mnhw", it->second->hardwareVersion);
		rep.getValue("mnfv", it->second->firmwareVersion);

		std::string manufacturerName = " manufacturerName "
				+ it->second->manufacturerName;
		std::string osName = " osName " + it->second->osName;
		std::string hardwareVersion = " hardwareVersion "
				+ it->second->hardwareVersion;
		std::string firmwareVersion = " firmwareVersion "
				+ it->second->firmwareVersion;
	}

}

void platformInfo()
{
	cout << "In function platformInfo " << endl;
	std::string platformDiscoveryURI = "/oic/p";
	OCPlatform::getPlatformInfo("", platformDiscoveryURI, connectivityType,
			&receivedPlatformInfo);
}

void Discovery_all()
{

	t_thingsmap.clear();
	cout << "In function Discovery_all " << endl;
	deviceInfo();
	platformInfo();
	resource_discovery();
	std::cout << " after Discovery_all " << '\n';
}

void t_things_garbage()
{
	for (auto it = t_thingsmap.cbegin(); it != t_thingsmap.cend(); ++it) {
		if (!(it->second->manufacturerName.length())) {
			t_thingsmap.erase(it->first);
		}
	}
}

void t_things_device ()
{
        std::cout << " \nIN t_things_device : " << '\n';

        for (auto it_uid = t_thingsmap.cbegin (); it_uid != t_thingsmap.cend ();
             ++it_uid)
        {
                int i = 0;
                for (auto it_key = t_resourceColmap.cbegin ();
                     it_key != t_resourceColmap.cend (); ++it_key)
                {
                        if (!(it_key->first.find (it_uid->first)))
                        {
                                t_device *device = new t_device;


                                device->Coluri = it_key->first;
                                 for (auto it_key = t_resourcemap.cbegin ();
                                     it_key != t_resourcemap.cend (); ++it_key)
                                {
                                        if (!(it_key->first.find (device->Coluri)))
                                        {
                                            device->resMap.insert (pair<string, t_resource*> (it_key->first,
                                                                                   it_key->second));
                                        }
                                }

                                device->setresourcecallback();
                                it_uid->second->deviceVec.push_back (device);
                                i++;
                        }
                }
                it_uid->second->setTdeviceCall();
        }
}


void t_resource_att_details()
{
	QueryParamsMap test;
	for (auto it_uid = t_thingsmap.cbegin(); it_uid != t_thingsmap.cend();
			++it_uid) {
		for (int i = 0; i < it_uid->second->deviceVec.size(); i++) {
			for (auto it_non = it_uid->second->deviceVec[i]->resMap.cbegin();
					it_non != it_uid->second->deviceVec[i]->resMap.cend();
					++it_non) {
				std::cout << "get req sent :"
						<< it_non->second->resource->get(test,
								std::bind(&t_resource::onGet, it_non->second,
										_1, _2, _3)) << endl;
				usleep(30000);
			}
		}
	}
}

void end_Discovery(int sig)
{
	reqType = "discovery";
	std::cout << " \n in end_Discovery map: " << '\n';
	std::cout << " \n Non_Collection Map: " << '\n';
	for (auto it = t_resourcemap.cbegin(); it != t_resourcemap.cend(); ++it) {
		std::cout << it->first << "\t:\t" << it->second << "\n";
	}
	std::cout << " \n Collection Map: " << '\n';
	for (auto it = t_resourceColmap.cbegin(); it != t_resourceColmap.cend();
			++it) {
		std::cout << it->first << "\t:\t" << it->second << "\n";
	}

	t_things_garbage();
	t_things_device();
	t_resource_att_details();

	if (auto_Discovery == true)
	{
		signal(SIGALRM, Auto_Discovery);
		alarm(3600);
	}
	else
	{
		signal(SIGALRM, SIG_IGN);
	}
}
